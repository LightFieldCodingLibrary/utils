function [psnr_y,psnr_yuv,psnr_u,psnr_v,ssim_y,ssim_yuv,ssim_u,ssim_v] = ...
    qualityMetrics(rec,ref,n,varargin)
%QUALITYMETRICS Summary of this function goes here
%   Detailed explanation goes here

if ~exist('n','var')
    dr = diff(getrangefromclass(rec));
    n = log2(double(dr)+1);
else
    dr = 2^n-1;
end

compute_ssim = nargout>4;

numchan = size(rec,3);

ssim_y = nan;
ssim_yuv = nan;
ssim_u = nan;
ssim_v = nan;

if numchan==1
    psnr_y = psnr(rec(:,:,1),ref(:,:,1),dr);
    psnr_u = nan;
    psnr_v = nan;
    
    psnr_yuv = nan;
    
    if compute_ssim
        ssim_y = ssim(rec(:,:,1),ref(:,:,1),'DynamicRange',dr);
    end
elseif numchan==3
    rec = utils.rgb2ycbcr(rec,n,varargin{:});
    ref = utils.rgb2ycbcr(ref,n,varargin{:});
    
    psnr_y = psnr(rec(:,:,1),ref(:,:,1),dr);
    psnr_u = psnr(rec(:,:,2),ref(:,:,2),dr);
    psnr_v = psnr(rec(:,:,3),ref(:,:,3),dr);
    
    psnr_yuv = (6*psnr_y + psnr_u + psnr_v)/8;
    
    if compute_ssim
        ssim_y = ssim(rec(:,:,1),ref(:,:,1),'DynamicRange',dr);
        ssim_u = ssim(rec(:,:,2),ref(:,:,2),'DynamicRange',dr);
        ssim_v = ssim(rec(:,:,3),ref(:,:,3),'DynamicRange',dr);
        
        ssim_yuv = ssim(rec,ref,'DynamicRange',dr);
    end
else
    error('Unexpected number of channels');
end

end