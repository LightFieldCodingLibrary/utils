function outMat = recast(inMat,varargin)
%RECAST Summary of this function goes here
%   Detailed explanation goes here

p = inputParser;
p.addOptional('outTypename', class(inMat), @ischar);
p.addOptional( 'inTypename', class(inMat), @ischar);

p.parse(varargin{:});

 inTypename = p.Results. inTypename;
outTypename = p.Results.outTypename;

  inRange                = utils.getRange( inTypename);
[outRange,~,outTypename] = utils.getRange(outTypename);

outMat = outRange(1)+diff(outRange)*(double(inMat)-inRange(1))/diff(inRange);
outMat = cast(outMat,outTypename);

end