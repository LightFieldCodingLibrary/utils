function [Cols,Labs,Offs,varargout] = split(Col,Lab,Off,varargin)
%SPLIT Summary of this function goes here
%   Detailed explanation goes here

nvararg = nargin-3;
numDim = numel(Off);
numLab = max(Lab(:));

[Cols,Labs,Offs,varargout{1:nvararg}] = deal(cell(1,numLab));

Offs(:) = deal({Off});
Lab = Lab.*ones(size(Col));

for lab = 1:numLab
    if ~any(Lab(:)==lab)
        continue
    end
    
    [Cols{lab},gv] = utils.tighten(Col,Lab==lab);
    Labs{lab} = Lab(gv{:});
    gv(end+1:numDim) = deal({1});
    Offs{lab} = Off-1+cellfun(@(v) v(1),gv);
    
    for it = 1:nvararg
        varargout{it}{lab} = varargin{it}(gv{:});
    end
end

end