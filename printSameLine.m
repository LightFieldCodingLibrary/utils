function printSameLine(varargin)
%PRINTSAMELINE Summary of this function goes here
%   Detailed explanation goes here
persistent sizeMsg

if isempty(sizeMsg)
    sizeMsg=0;
end

if nargin==0
    fprintf('\n');
    sizeMsg=0;
else
    if sizeMsg>0
        fprintf(repmat('\b',1,sizeMsg));
    end
    msg = varargin{1};
    fprintf(msg);
    sizeMsg = numel(msg);
end
end

