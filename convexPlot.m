function [bitrate,peaksnr] = convexPlot(bitrate,peaksnr,varargin)
%CONVEX_BITRATE_PSNR Compute and plot upper convex hull of 2D
%points
%   Detailed explanation goes here

[bitrate,peaksnr] = utils.convexEnvelope(bitrate,peaksnr);
plot(bitrate,peaksnr,varargin{:});
end