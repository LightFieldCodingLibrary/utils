function m = getMat(filename,isWritable)
%GET_MAT Summary of this function goes here
%   Detailed explanation goes here

folder = fileparts(filename);

if isWritable && ~exist(folder,'dir')
    mkdir(folder);
end

namemat = [filename,'.mat'];
m = matfile(namemat,'Writable',isWritable);
end