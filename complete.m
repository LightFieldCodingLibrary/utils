function [M,Mask] = complete(M,varargin)
%COMPLETE Summary of this function goes here
%   Detailed explanation goes here

Dir = mfilename('fullpath');
Dir = fileparts(Dir); 
addpath(genpath(fullfile(Dir,'LRTC')));

p = inputParser; p.StructExpand = true; p.KeepUnmatched = true;
p.addParameter('Method', 'rank', @ischar);
p.parse(varargin{:});

Method = p.Results.Method;

maxIter=500;
switch Method
    case 'trace'
        alpha=[1,1]; betaMult=1.1 ; epsilon=1e-3; TraceNormToRankParam=0;
    case 'rank'
        alpha=[1,1]; betaMult=1.44; epsilon=1e-3; TraceNormToRankParam=inf;
    otherwise
        error('Unknown completion method');
end

Mask = ~isnan(M);

p.addParameter('Mask'                , Mask                , @islogical);
p.addParameter('alpha'               , alpha               , @isnumeric);
p.addParameter('betaMult'            , betaMult            , @isnumeric);
p.addParameter('maxIter'             , maxIter             , @isnumeric);
p.addParameter('epsilon'             , epsilon             , @isnumeric);
p.addParameter('TraceNormToRankParam', TraceNormToRankParam, @isnumeric);
p.parse(varargin{:});

Mask                 = p.Results.Mask;
alpha                = p.Results.alpha;
betaMult             = p.Results.betaMult;
maxIter              = p.Results.maxIter;
epsilon              = p.Results.epsilon;
TraceNormToRankParam = p.Results.TraceNormToRankParam;

M(~Mask) = 0;

M = LRTCADMrho(M,Mask,alpha,betaMult,maxIter,epsilon,TraceNormToRankParam);

Mask = repmat(any(Mask,2),1,size(M,2));
M(~Mask) = NaN;

end