function [typename,nbits,casttypename,castnbits] = precision(typename)
%PRECISION Summary of this function goes here
%   Detailed explanation goes here

expression = '(?<type>\D+)(?<nbits>\d*)';

names = regexp(typename,expression,'names');

typename = names.type;
nbits = str2double(names.nbits);

if isnan(nbits)
    switch typename
        case {'float','single'}
            nbits = 32;
        case 'double'
            nbits = 64;
        otherwise
            error('Incorrect input type');
    end
end

integernbits = [8,16,32,64];
integernbits = integernbits(integernbits>=nbits);
castnbits = num2str(integernbits(1));

switch typename
    case {'ubit','uint'}
        casttypename = ['uint',castnbits];
    case { 'bit', 'int'}
        casttypename = [ 'int',castnbits];
    otherwise
        casttypename = typename;
end

end