function [name,imgSize,imgRes,subSamp,bitDepth] = name2params(convname,ext)
%NAME2PARAMS Summary of this function goes here
%   Detailed explanation goes here

convexpr = ['(?<name>\w+)_(?<imgRes>\d+x\d+)_(?<imgSize>\d+x\d+)_',...
    '(?<subSamp>400|420|422|444)_(?<bitDepth>\d+)bit.',ext];

tokens = regexp(convname,convexpr,'names');

name = tokens.name;
imgSize  = strsplit(tokens.imgSize,'x');
imgSize  = cellfun(@str2double,imgSize);
imgSize  = flip(imgSize);
imgRes   = strsplit(tokens.imgRes,'x');
imgRes   = cellfun(@str2double,imgRes);
subSamp  = tokens.subSamp;
bitDepth = str2double(tokens.bitDepth);

switch subSamp
    case '400'
        imgSize = [imgSize,1];
    otherwise
        imgSize = [imgSize,3];
end

end