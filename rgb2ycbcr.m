function img = rgb2ycbcr(img,n,fullRange,conv2int)
%RGB2YCBCR Summary of this function goes here
%   Detailed explanation goes here

%  Recommendation ITU-R BT.709-6
M = [ 0.212600   0.715200  0.072200 ;
     -0.114572  -0.385428  0.500000 ;
      0.500000  -0.454153 -0.045847];

if (nargin < 2)
    dr = diff(getrangefromclass(img));
    n = log2(double(dr)+1);
else
    dr = double(2^n-1);
end

if (nargin < 3); fullRange = true; end
if (nargin < 4); conv2int = isinteger(img); end

if fullRange
    scaleY = 255; offsetY = 0 ; scaleC = 255; offsetC = 128;
else
    scaleY = 219; offsetY = 16; scaleC = 224; offsetC = 128;
end

img        = double(img);
img        = reshape( reshape( img,[],3 )*M' ,size(img));
img(:,:,1) = (scaleY*img(:,:,1) + dr*offsetY)/255;
img(:,:,2) = (scaleC*img(:,:,2) + dr*offsetC)/255;
img(:,:,3) = (scaleC*img(:,:,3) + dr*offsetC)/255;

if conv2int
    if(n<=8)
        img = uint8(img);
    elseif (n<=16)
        img = uint16(img);
    elseif (n<=32)
        img = uint32(img);
    elseif (n<=64)
        img = uint64(img);
    else
        print('invalid bit depth')
    end
end

end