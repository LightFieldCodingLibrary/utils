function labels = colorLabels(labels,varargin)
%COLORLABELS Summary of this function goes here
%   Detailed explanation goes here

sz = size(labels);
cmap = jet(max(labels(:))+1);
shuffle = false;
gray = ones(sz);
coldim = 3;

p = inputParser();

p.addParameter('cmap'   , cmap   , @isnumeric);
p.addParameter('shuffle', shuffle, @(x) isnumeric(x)||islogical(x));
p.addParameter('gray'   , gray   , @isnumeric);
p.addParameter('coldim' , coldim , @isnumeric);

p.parse(varargin{:});

cmap    = p.Results.cmap;
shuffle = p.Results.shuffle;
gray    = p.Results.gray;
coldim  = p.Results.coldim;

if shuffle
    rng('default');
    sigma = randperm(size(cmap,1));
    cmap = cmap(sigma,:);
end

order = 1:(ndims(gray)+1);
order(coldim) = ndims(gray)+1;
order(ndims(gray)+1) = coldim;

labels = cmap(labels(:)+1,:);
labels = reshape(labels,[sz,3]);
labels = permute(labels,order);
labels = labels.*gray;

end