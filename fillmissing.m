function Value = fillmissing(Value,se,varargin)
%FILLMISSINF Extend signal using structuring element se numIter times
%   Detailed explanation goes here

if nargin>2
    numIter = varargin{1};
else
    numIter = +inf;
end

Mask = isnan(Value);
it = 0;
while any(Mask(:)) && it<numIter
    Mask = isnan(Value);
    Temp = Value;
    Temp(Mask)=-Inf;
    Temp = imdilate(Temp,se);
    Temp(isinf(Temp))=NaN;
    Value(Mask) = Temp(Mask);
    it = it+1;
end
end