function t = triangulation(X,Y,varargin)
%TRIANGULATION Summary of this function goes here
%   Detailed explanation goes here

I = (1:numel(X))';
p = inputParser();
p.addOptional('I', I, @isnumeric);

p.parse(varargin{:});
I = p.Results.I;

sz = size(X);

[X_,Y_] = ndgrid(1:(sz(1)-1),1:(sz(2)-1));
TX = reshape( (X_(:)+[0,1,0,1,0,1])' ,3,[])';
TY = reshape( (Y_(:)+[0,0,1,1,1,0])' ,3,[])';

T = sub2ind(sz,TX,TY);

Ii(I) = 1:numel(I);

t = triangulation(Ii(T),X(I(:)),Y(I(:)));

end